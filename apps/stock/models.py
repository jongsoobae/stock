from __future__ import annotations

from datetime import date, datetime
from typing import Iterable, List, Optional

import requests
from djongo import models

from apps.stock.utils.date import OptDate, OptDateTime, parse_date, parse_datetime


class Code(models.Model):
    code = models.CharField("종목 코드", primary_key=True, max_length=32)
    name = models.CharField("종목 명", max_length=32)

    def delete(self, using=None, keep_parents=False):
        return NotImplemented

    def to_dict(self):
        return {"code": self.code, "name": self.name}


class DailyTotalNetBid(models.Model):
    class InvestorsType(models.TextChoices):
        FOREIGN = "9000", "외국인"
        FOREIGN_ETC = "9001", "기타외국인"
        GOV_TOTAL = "7050", "기관합계"
        PERSONAL = "8000", "개인"

    URL = "http://data.krx.co.kr/comm/bldAttendant/getJsonData.cmd"
    STATIC_PARAMS = {
        "bld": "dbms/MDC/STAT/standard/MDCSTAT02401",
        "mktId": "ALL",
        "invstTpCd": InvestorsType.GOV_TOTAL,
        "share": 1,
        "money": 1,
        "csvxls_isNo": False,
    }

    code = models.EmbeddedField(model_container=Code)
    collect_date = models.DateField(verbose_name="수집일")

    personal_ask_val = models.IntegerField(verbose_name="개인 매도", default=0)
    personal_bid_val = models.IntegerField(verbose_name="개인 매수", default=0)

    gov_ask_val = models.IntegerField(verbose_name="기관 매도", default=0)
    gov_bid_val = models.IntegerField(verbose_name="기관 매수", default=0)

    f_ask_val = models.IntegerField(verbose_name="외국인 매도", default=0)
    f_bid_val = models.IntegerField(verbose_name="외국인 매수", default=0)
    f_etc_ask_val = models.IntegerField(verbose_name="기타외국인 매도", default=0)
    f_etc_bid_val = models.IntegerField(verbose_name="기타외국인 매수", default=0)

    f_net_bid_val = models.IntegerField(verbose_name="외국인 순매수", default=0)
    f_etc_net_bid_val = models.IntegerField(verbose_name="기타외국인 순매수", default=0)
    # net_bid
    personal_net_bid_val = models.IntegerField(verbose_name="개인 순매수", default=0)
    gov_net_bid_val = models.IntegerField(verbose_name="기관 순매수", default=0)
    f_total_net_bid_val = models.IntegerField(verbose_name="총외국인 순매수", default=0)
    fg_net_bid_val = models.IntegerField(verbose_name="외국인+기관 순매수", default=0)
    total_net_bid_val = models.IntegerField(verbose_name="총합 순매수", default=0)

    class Meta:
        unique_together = ("code", "collect_date")

    def __str__(self):
        return f"{self.code['name']},{self.collect_date} {self.gov_net_bid_val} {self.f_total_net_bid_val} {self.total_net_bid_val}, {self.fg_rate()}"

    def save(self, *args, **kwargs):
        p = int(self.personal_net_bid_val)
        gov = int(self.gov_net_bid_val)
        f = self.f_total_net_bid_val = int(self.f_net_bid_val) + int(self.f_etc_net_bid_val)
        self.fg_net_bid_val = int(gov + f)
        self.total_net_bid_val = int(gov + f + p)
        super().save(*args, **kwargs)

    @classmethod
    def get_dynamic_param(cls, day, investors_type: InvestorsType):
        return {
            "invstTpCd": investors_type,
            "strtDd": f"{day: %Y%m%d}",
            "endDd": f"{day: %Y%m%d}",
        }

    @classmethod
    def request_data(
        cls,
        day: OptDate = None,
        investors_type: InvestorsType = InvestorsType.GOV_TOTAL,
    ) -> List:
        day = parse_date(day or date.today())
        return requests.post(
            cls.URL,
            data={**cls.STATIC_PARAMS, **cls.get_dynamic_param(day, investors_type)},
        ).json()["output"]

    @classmethod
    def collected(cls, day: OptDate = None):
        day = parse_date(day or date.today())
        return cls.objects.filter(collect_date=day)

    @classmethod
    def collect(cls, day: OptDate = None):
        day = parse_date(day or date.today())
        if collected := cls.collected(day):
            return collected, False
        else:
            collected.delete()

        g_data = cls.request_data(day, cls.InvestorsType.GOV_TOTAL)
        f_data = cls.request_data(day, cls.InvestorsType.FOREIGN)
        f_etc_data = cls.request_data(day, cls.InvestorsType.FOREIGN_ETC)
        p_data = cls.request_data(day, cls.InvestorsType.PERSONAL)

        bulk_create_dict = {}
        cls._set_data(day, g_data, cls.InvestorsType.GOV_TOTAL, bulk_create_dict)
        cls._set_data(day, f_data, cls.InvestorsType.FOREIGN, bulk_create_dict)
        cls._set_data(day, f_etc_data, cls.InvestorsType.FOREIGN_ETC, bulk_create_dict)
        cls._set_data(day, p_data, cls.InvestorsType.PERSONAL, bulk_create_dict)

        for instance in bulk_create_dict.values():
            instance.save()
        return cls.collected(day), True

    @classmethod
    def _set_data(
        cls,
        day: OptDate,
        data: Iterable,
        investors_type: InvestorsType,
        bulk_create_dict,
    ):
        for datum in data:
            code_str = datum["ISU_SRT_CD"]
            code_name = datum["ISU_NM"]
            code, _ = Code.objects.get_or_create(code=code_str, name=code_name)
            instance = bulk_create_dict.get(code_str)
            if not instance:
                instance = cls(collect_date=day, code=code.to_dict())

            if investors_type == cls.InvestorsType.GOV_TOTAL:
                instance.gov_ask_val = datum["ASK_TRDVAL"].replace(",", "")
                instance.gov_bid_val = datum["BID_TRDVAL"].replace(",", "")
                instance.gov_net_bid_val = datum["NETBID_TRDVAL"].replace(",", "")
            elif investors_type == cls.InvestorsType.FOREIGN:
                instance.f_ask_val = datum["ASK_TRDVAL"].replace(",", "")
                instance.f_bid_val = datum["BID_TRDVAL"].replace(",", "")
                instance.f_net_bid_val = datum["NETBID_TRDVAL"].replace(",", "")
            elif investors_type == cls.InvestorsType.FOREIGN_ETC:
                instance.f_etc_ask_val = datum["ASK_TRDVAL"].replace(",", "")
                instance.f_etc_bid_val = datum["BID_TRDVAL"].replace(",", "")
                instance.f_etc_net_bid_val = datum["NETBID_TRDVAL"].replace(",", "")
            elif investors_type == cls.InvestorsType.PERSONAL:
                instance.personal_ask_val = datum["ASK_TRDVAL"].replace(",", "")
                instance.personal_bid_val = datum["BID_TRDVAL"].replace(",", "")
                instance.personal_net_bid_val = datum["NETBID_TRDVAL"].replace(",", "")

            bulk_create_dict[code_str] = instance

    def fg_rate(self):
        f = int(self.f_total_net_bid_val)
        g = int(self.gov_net_bid_val)
        total = f + g
        return (
            int((f / total) * 100),
            100 - int((f / total) * 100),
        )

    def get_fg_rate_display(self):
        f, g = self.fg_rate()
        return f"({f} / {g})"


class MarketPrice(models.Model):
    code = models.EmbeddedField(model_container=Code)
    collect_datetime = models.DateTimeField(verbose_name="수집시간")
    value = models.IntegerField(verbose_name="현재 가격", default=0)
    market_value = models.IntegerField(verbose_name="시가총액", default=0)
    fluctuations_ratio = models.FloatField(verbose_name="등락율", default=0.0)
    trading_volume = models.IntegerField(verbose_name="거래량", default=0)
    trading_value = models.IntegerField(verbose_name="거래대금", default=0)
    # additional
    trading_ratio = models.FloatField(verbose_name="시총대비 거래대금", default=0.0)

    URL = "https://m.stock.naver.com/api/json/sise/siseListJson.nhn?menu=market_sum&sosok=0&pageSize=200&page=1"

    def normalized_collected_datetime(self: Optional[MarketPrice], dt: OptDateTime = None):
        dt = parse_datetime(dt or datetime.now())
        return datetime(*dt.timetuple()[:5], tzinfo=dt.tzinfo)

    @classmethod
    def request_data(cls) -> List:
        return requests.get(cls.URL,).json()[
            "result"
        ]["itemList"]

    @classmethod
    def collected(cls, dt: OptDateTime = None):
        dt = parse_datetime(dt or datetime.now())
        return cls.objects.filter(collect_datetime=cls.normalized_collected_datetime(None, dt))

    @classmethod
    def collect(cls):
        dt = cls.normalized_collected_datetime(None)
        if collected := cls.collected(dt):
            return collected, False
        else:
            collected.delete()

        for instance in cls._collect(dt):
            instance.save()
        return cls.collected(dt), True

    @classmethod
    def _collect(cls, normalized_dt: datetime):
        for datum in cls.request_data():
            code_str = datum["cd"]
            code_name = datum["nm"]
            code, _ = Code.objects.get_or_create(code=code_str, name=code_name)

            yield cls(
                code=code.to_dict(),
                collect_datetime=normalized_dt,
                value=datum["nv"],
                market_value=int(datum["mks"]) * 100_000_000,
                fluctuations_ratio=float(datum["cr"]),
                trading_volume=datum["aq"],
                trading_value=int(datum["aa"]) * 1_000_000,
            )

    def save(self, *args, **kwargs):
        self.trading_ratio = round(self.trading_value / self.market_value * 100, 2)
        super().save(*args, **kwargs)
