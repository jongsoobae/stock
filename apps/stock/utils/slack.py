import requests

SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"  # YGY BROS - STOCK
SLACK_WEBHOOK_URL2 = "https://hooks.slack.com/services/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"  # YGY BROS - BOT


def send_to_slack_channel_stock(title, message, is_test=True):
    url = SLACK_WEBHOOK_URL2 if is_test else SLACK_WEBHOOK_URL
    data = message_dict(title, message)
    return requests.post(url, json=data)


def message_dict(title, message):
    return {
        "blocks": [
            {
                "type": "header",
                "text": {"type": "plain_text", "text": title, "emoji": True},
            },
            {"type": "section", "text": {"type": "mrkdwn", "text": message}},
        ]
    }
