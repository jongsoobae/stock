import datetime
from typing import Optional, Union

from django.db.models import DateField, DateTimeField, TimeField

TimeOrStr = Union[str, datetime.time]
DateOrStr = Union[datetime.date, str]
DateTimeOrStr = Union[datetime.datetime, str]

OptTime = Optional[TimeOrStr]
OptDate = Optional[DateOrStr]
OptDateTime = Optional[DateTimeOrStr]


def parse_time(value: TimeOrStr) -> datetime.time:
    return TimeField().get_prep_value(value)


def parse_date(value: DateOrStr) -> datetime.date:
    return DateField().get_prep_value(value)


def parse_datetime(value: DateTimeOrStr) -> datetime.datetime:
    return DateTimeField().get_prep_value(value)
