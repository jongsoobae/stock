from django.core.management import BaseCommand

from apps.stock.models import MarketPrice

DATE_FORMAT_STR = "%Y-%m-%d"
SLICING = 10


def sorter(mp: MarketPrice):
    return mp.fluctuations_ratio


class Command(BaseCommand):
    def handle(self, *args, **options):
        collected, _ = MarketPrice.collect()
        for i, mp in enumerate(collected.order_by("-trading_ratio")):
            mp.cr_rank = i + 1
            mp.save()
