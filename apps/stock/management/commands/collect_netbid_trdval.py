from datetime import datetime
from typing import Iterable, Tuple

from click import DateTime
from django.core.management import BaseCommand

from apps.stock.models import DailyTotalNetBid
from apps.stock.utils import slack

DATE_FORMAT_STR = "%Y-%m-%d"
SLICING = 10


def high_trdval_by(items):
    return (
        items.order_by("-f_total_net_bid_val").values_list("code", "f_total_net_bid_val")[:SLICING],
        items.order_by("-gov_net_bid_val").values_list("code", "gov_net_bid_val")[:SLICING],
        items.order_by("-fg_net_bid_val")[:SLICING],
    )


def build_slack_message(values) -> Iterable[Tuple]:
    return "\n".join(f"{i + 1}. {x[0]['name']} {x[1]:,}" for i, x in enumerate(values))


def build_slack_message_all(values: Iterable[DailyTotalNetBid]):
    return "\n".join(f"{i + 1}. {x.code['name']} {x.fg_net_bid_val:,} {x.get_fg_rate_display()}" for i, x in enumerate(values))


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--dt", type=DateTime(formats=[DATE_FORMAT_STR]), default=datetime.now())
        parser.add_argument("-fn", "--force-notify", action="store_true")
        parser.add_argument("--test", action="store_true")

    def handle(self, *args, **options):
        dt = options["dt"]
        fn = options["force_notify"]
        test = options["test"]

        output, collected = DailyTotalNetBid.collect(day=dt)
        if not collected and not fn:
            return

        f, g, a = high_trdval_by(output)

        slack.send_to_slack_channel_stock(
            title=f"외국인+기관 순매수 상위 ({dt: %Y-%m-%d %H:%M})",
            message=build_slack_message_all(a),
            is_test=test,
        )
        slack.send_to_slack_channel_stock(
            title=f"외국인 순매수 상위 ({dt: %Y-%m-%d %H:%M})",
            message=build_slack_message(f),
            is_test=test,
        )
        slack.send_to_slack_channel_stock(
            title=f"기관 순매수 상위 ({dt: %Y-%m-%d %H:%M})",
            message=build_slack_message(g),
            is_test=test,
        )
